namespace MealSelector.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using MealSelector.DAL;
    using WebMatrix.WebData;
    using System.Web.Security;

    internal sealed class Configuration : DbMigrationsConfiguration<SelectorContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(SelectorContext context)
        {
            SeedMembership();
        }

        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("SelectorConnection",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists("Administrator"))
                Roles.CreateRole("Administrator");

            if (!Roles.RoleExists("User"))
                Roles.CreateRole("User");

            if (!WebSecurity.UserExists("Paul"))
                WebSecurity.CreateUserAndAccount(
                    "Paul",
                    "password",
                    new { Email = "paulmurphygb@gmail.com" });

            if (!Roles.GetRolesForUser("Paul").Contains("Administrator"))
                Roles.AddUsersToRoles(new[] { "Paul" }, new[] { "Administrator" });
        }
    }
}
