﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebMatrix.WebData;
using System.Web.Security;

namespace MealSelector.DAL
{
    public class SelectorDbInit : CreateDatabaseIfNotExists<SelectorContext>
    {
        protected override void Seed(SelectorContext context)
        {
            context.Meals.Add(new Models.Meal { Name = "Test Meal", Date = DateTime.Now });
            SeedMembership();
        }

        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("SelectorConnection",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists("Administrator"))
                Roles.CreateRole("Administrator");

            if (!Roles.RoleExists("User"))
                Roles.CreateRole("User");

            if (!WebSecurity.UserExists("Paul"))
                WebSecurity.CreateUserAndAccount(
                    "Paul",
                    "password",
                    new { Email = "paulmurphygb@gmail.com" });

            if (!Roles.GetRolesForUser("Paul").Contains("Administrator"))
                Roles.AddUsersToRoles(new[] { "Paul" }, new[] { "Administrator" });
        }
    }
}