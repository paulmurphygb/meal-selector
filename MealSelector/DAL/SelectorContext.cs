﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MealSelector.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MealSelector.DAL
{
    public class SelectorContext : UsersContext
    {
        public DbSet<Meal> Meals { get; set; }

        public DbSet<UserVote> Votes { get; set; }

        public DbSet<WinningVote> WinningVotes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}