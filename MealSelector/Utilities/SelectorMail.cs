﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using SendGridMail.Transport;
using SendGridMail;
using System.Net;
using System.Configuration;

namespace MealSelector.Utilities
{
    public class SelectorMail
    {
        private string username = ConfigurationManager.AppSettings["SendGridUsername"].ToString();
        private string password = ConfigurationManager.AppSettings["SendGridPassword"].ToString();

        public string From { get; set; }

        public string To { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }

        public string Subject { get; set; }

        public SelectorMail(string from, string to, string name, string message, string subject)
        {
            From = from;
            To = to;
            Name = name;
            Message = message;
            Subject = subject;
        }

        public void Send()
        {
            Send(From, To, Name, Message, Subject);
        }

        public void Send(string from, string to, string name, string message, string subject)
        {
            var sendFrom = new MailAddress(from, name);
            var sendTo = new MailAddress(to);
            var email = SendGrid.GetInstance();
            email.To = new MailAddress[] { sendTo };
            email.From = sendFrom;
            email.Subject = subject;
            email.Html = message;
            var credentials = new NetworkCredential(username, password);
            var transport = SMTP.GetInstance(credentials);
            transport.Deliver(email);
        }
    }
}