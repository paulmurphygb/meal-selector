﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MealSelector.Models;
using MealSelector.DAL;
using WebMatrix.WebData;

namespace MealSelector.Controllers
{
    public class MealsController : Controller
    {
        private SelectorContext db = new SelectorContext();

        //
        // GET: /Meals/

        public ActionResult Index(string message)
        {
            ViewBag.Message = message;
            return View(db.Meals.ToList());
        }

        //
        // GET: /Meals/Vote/5
        [Authorize]
        public ActionResult Vote(int id = 0)
        {
            UserVote vote;
            Meal meal = db.Meals.Find(id);
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            UserProfile user = db.UserProfiles.SingleOrDefault(x => x.UserId == userId);
            using (db)
            {
                db.Votes.Include("Meal");
                db.Votes.Include("User");
                vote = db.Votes.SingleOrDefault(x => x.UserId == userId && x.MealId == meal.MealId);
                if (vote == null)
                {
                    vote = new UserVote { MealId = meal.MealId, UserId = userId };
                    db.Votes.Add(vote);
                    db.SaveChanges();
                }
            }
            if (vote == null)
            {
                return HttpNotFound();
            }
            return View(vote);
        }

        [HttpPost]
        public ActionResult Vote(UserVote vote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vote).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { message = "Thanks for voting!" });
            }
            return View(vote);
        }

        //
        // GET: /Meals/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Meals/Create

        [HttpPost]
        public ActionResult Create(Meal meal)
        {
            if (ModelState.IsValid)
            {
                db.Meals.Add(meal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(meal);
        }

        //
        // GET: /Meals/Decide/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Decide(int id = 0)
        {
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            else if (meal.WinningVoteId != null)
            {
                return RedirectToAction("Winner", new { id = meal.MealId });
            }
            return View(meal);
        }

        //
        // POST: /Meals/Edit/5

        [HttpPost]
        public ActionResult Decide(Meal meal)
        {
            if (ModelState.IsValid && meal != null)
            {
                if (meal.WinningVoteId != null) return RedirectToAction("Winner", new { id = meal.MealId });
                List<UserVote> votes = db.Votes.Where(x => x.MealId == meal.MealId && !string.IsNullOrEmpty(x.Details)).ToList();
                Random random = new Random();
                int index = random.Next(votes.Count);
                UserVote winner = votes[index];        
                using (db) 
                {
                        WinningVote winningVote = new WinningVote();
                        winningVote.UserVoteId = winner.UserVoteId;
                        db.WinningVotes.Add(winningVote);
                        db.Entry(winningVote).State = EntityState.Added;
                        meal.WinningVoteId = winningVote.WinningVoteId;
                        db.Entry(meal).State = EntityState.Modified;
                        db.SaveChanges();
                }
                return RedirectToAction("Winner", new { id = meal.MealId });
            }
            return View(meal);
        }

        public ActionResult Winner(int id = 0)
        {
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            else if (meal.WinningVoteId == null)
            {
                return RedirectToAction("index");
            }
            else
            {
                WinningVote vote = db.WinningVotes.Include("UserVote").Include("UserVote.User").SingleOrDefault(x => x.WinningVoteId == meal.WinningVoteId);
                return View(vote);
            }
        }

        //
        // GET: /Meals/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id = 0)
        {
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            return View(meal);
        }

        //
        // POST: /Meals/Edit/5

        [HttpPost]
        public ActionResult Edit(Meal meal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(meal);
        }

        //
        // GET: /Meals/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id = 0)
        {
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            return View(meal);
        }

        //
        // POST: /Meals/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Meal meal = db.Meals.Find(id);
            var winningVote = db.WinningVotes.SingleOrDefault(x=>x.WinningVoteId == meal.WinningVoteId);
            if(winningVote != null) db.WinningVotes.Remove(winningVote);
            var votes = db.Votes.Where(x => x.MealId == meal.MealId).ToList();
            if (votes.Count > 0)
            {
                foreach (var vote in votes)
                {
                    db.Votes.Remove(vote);
                }
            }
            db.Meals.Remove(meal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}