﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MealSelector.Models
{
    public class Meal
    {
        [Key]
        public int MealId { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public string Details { get; set; }

        public int? WinningVoteId { get; set; }

        [ForeignKey("WinningVoteId")]
        public WinningVote WinningVote { get; set; }
    }
}