﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MealSelector.Models
{
    public class WinningVote
    {
        [Key]
        public int WinningVoteId { get; set; }

        public int UserVoteId { get; set; }

        [ForeignKey("UserVoteId")]
        public UserVote UserVote { get; set; }
    }
}