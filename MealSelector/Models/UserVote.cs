﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MealSelector.Models
{
    public class UserVote
    {
        [Key]
        public int UserVoteId { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public UserProfile User { get; set; }

        public int MealId { get; set; }

        [ForeignKey("MealId")]
        public Meal Meal { get; set; }

        public string Details { get; set; }
    }
}